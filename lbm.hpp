/*================================== C++ ======================================*\
|                                                                               |
|                           1D Lattice-Boltzmann                                |
|                               Milo Feinberg                                   |
|                                                                               |
\*=============================================================================*/

class lbm
{
public:
    lbm(const double Length, const int Npts, const double speedOfSound, const double viscosity ):
        L(Length),
        N(Npts), 
        cs(speedOfSound), 
        nu(viscosity)
    {
        bufferCells = 1;                // number of buffer cells
        realN = N + 2*bufferCells;      // actual length of the arrays
        
        c = cs * std::sqrt(3);          // advection speed
        dX = L / (N-1);                 // distance between nodes
        dT = dX / c;                    // set the time step size
        beta = 1/(1+(2*nu)/(cs*cs));    // relaxation coef.
        
        // Allocate vectors
        X.resize(realN, 0.0);           // x-location of the nodes
        rho.resize(realN, 0.0);         // density
        U.resize(realN, 0.0);           // velocity
        fm.resize(realN, 0.0);          // population with velocity -c
        f0.resize(realN, 0.0);          // population with velocity  0
        fp.resize(realN, 0.0);          // population with velocity +c
        
        initializeDomain();
    }

    void advect() {
        // shift fp into the buffer
        for (int i = N; i >= 0; i--) {
            fp[node(i)] = fp[node(i-1)];
        }
        // shift fm into the buffer
        for (int i = -1; i <= N; i++) {
            fm[node(i)] = fm[node(i+1)];
        }
    }

    void applyBCs(int boundary) {
        switch(boundary) {
            case 1: 
                // periodic boundary conditions
                std::swap(fm[node(-1)], fm[node(N-1)]);
                std::swap(fp[node(N)], fp[node(0)]);
                break;
            case 2: 
                // bounce-back boundary conditions
                std::swap(fp[node(N)], fm[node(N-1)]);
                std::swap(fp[node(0)], fm[node(-1)]);
                break;
            case 3: 
                // Dirichlet boundary conditions
                fm[node(N-1)] = feq(1, rho[node(N-1)], U[node(N-1)]);
                fp[node(0)] = feq(3, rho[node(0)], U[node(0)]);
                break;

        }
    }
    
    void colide(bool navierStokes) {
        for (size_t i = 0; i < N; i++){
            // Calculate new macro properties
            rho[node(i)] = fm[node(i)] + f0[node(i)] + fp[node(i)];
            if (navierStokes == true) {
                U[node(i)] = (c*fp[node(i)] - c*fm[node(i)]) / rho[node(i)];
            }
            
            // Calculate new equilibrium populations
            fm[node(i)] += 2.0*beta*(feq(1, rho[node(i)], U[node(i)]) - fm[node(i)]);
            f0[node(i)] += 2.0*beta*(feq(2, rho[node(i)], U[node(i)]) - f0[node(i)]);
            fp[node(i)] += 2.0*beta*(feq(3, rho[node(i)], U[node(i)]) - fp[node(i)]);
        }
    }
        
    void setInitialConditions(int initialConditions) {
        
        switch (initialConditions) {
            case 1:
                for (int i=0; i<N; i++) {
                    // density step for Shock Tube
                    U[node(i)] = 0.0;                    
                    if (X[node(i)] < L/2) {
                        rho[node(i)] = 1.5;
                    } else {
                        rho[node(i)] = 1.0;
                    }
                    
                    fm[node(i)] = feq(1, rho[node(i)], U[node(i)]);
                    f0[node(i)] = feq(2, rho[node(i)], U[node(i)]);
                    fp[node(i)] = feq(3, rho[node(i)], U[node(i)]);
                }
                break;
            case 2:
                for (int i=0; i<N; i++) {
                    // steep Gaussian
                    U[node(i)] = 0.1;
                    rho[node(i)] = 1.0 + 0.5*std::exp(-5000.0*std::pow((X[node(i)]/L - 0.25),2));
                    
                    fm[node(i)] = feq(1, rho[node(i)], U[node(i)]);
                    f0[node(i)] = feq(2, rho[node(i)], U[node(i)]);
                    fp[node(i)] = feq(3, rho[node(i)], U[node(i)]);
                }
                break;
            case 3:
                for (int i=0; i<N; i++) {
                    // hyperbolic tangent
                    U[node(i)] = 0.1;
                    rho[node(i)] = 1.0 + 0.5*(1 - std::tanh((X[i]/L - 0.2)/0.01));
                    
                    fm[node(i)] = feq(1, rho[node(i)], U[node(i)]);
                    f0[node(i)] = feq(2, rho[node(i)], U[node(i)]);
                    fp[node(i)] = feq(3, rho[node(i)], U[node(i)]);
                }
                break;
        }
    }
        
    void writeOut(double step ) {
        double t = step * dT;
        std::string filename = std::to_string(t) + ".csv";
        std::ofstream outputfile;
        outputfile.open(filename);
        outputfile << "# t = " << t << "\n";
        outputfile << "N, X, rho, U\n";
        for (int i = 0; i < N; i++){
            outputfile << i << ", " << X[node(i)] << ", " << rho[node(i)] << ", " << U[node(i)] << "\n";
        }
    }

    void writeOutVerbose(double step ) {
        double t = step * dT;
        std::string filename = std::to_string(t) + ".csv";
        std::ofstream outputfile;
        outputfile.open(filename);
        outputfile << "# t = " << t << "\n";
        outputfile << "N, X, rho, U\n";
        for (int i = 0; i < N; i++){
            outputfile << i << ", " << X[node(i)] << ", " << rho[node(i)] << ", " << U[node(i)] << fm[node(i)] << ", " << f0[node(i)] << ", " << fp[node(i)] << "\n";
        }
    }
    
    int node(int i) {
        return i+1;
    }
    
private:
   
    void initializeDomain() {
        for (int i=0; i < realN; i++) {
            X[i] = (i - 1)*dX;
        }
    }
    
    double feq(int index, double rho, double U) {
        double Ma, f;
        Ma = U/cs;
        
        switch(index) {
            case 1:
                f = (1.0/3.0)*rho*(((-U*c - cs*cs)/(2.0*cs*cs)) + std::sqrt(1.0 + Ma*Ma));
                break;
            case 2:
                f = (2.0/3.0)*rho*(2.0 - std::sqrt(1.0 + Ma*Ma));
                break;
            case 3:
                f = (1.0/3.0)*rho*(((U*c - cs*cs)/(2.0*cs*cs)) + std::sqrt(1.0 + Ma*Ma));
                break;
        }
        return f;
    }

    double L, cs, nu, c, dX, dT, beta;
    int N, realN, bufferCells;
    std::vector<double> X, rho, U, fm, f0, fp;
    
    
};

// double c, double cs,
