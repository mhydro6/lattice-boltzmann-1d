/*================================== C++ ======================================*\
|                                                                               |
|                           1D Lattice-Boltzmann                                |
|                               Milo Feinberg                                   |
|                                                                               |
\*=============================================================================*/

#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <fstream>

#include "input.hpp"
#include "lbm.hpp"

int main()
{
    // initialize the simulation
    lbm sim(L, N, cs, nu);
    
    // set initial conditions
    sim.setInitialConditions(ICs);
    
    if (WRITE == true) {
            if (VERBOSE == true ) {
                sim.writeOutVerbose(0);
            } else {
                sim.writeOut(0);
            }
        }
    
    for (int n = 0; n <= nTS; n++) {
        sim.advect();
        sim.applyBCs(BCs);
        sim.colide(NSE);
        
        if (WRITE == true) {
            if (n % writeInterval == 0) {
                if (VERBOSE == true ) {
                    sim.writeOutVerbose(n);
                } else {
                    sim.writeOut(n);
                }
            }
        }
    }
    
    return 0;
}
