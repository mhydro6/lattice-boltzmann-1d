main: main.o 
	g++ -o main main.o
	
main.o: main.cpp input.hpp lbm.hpp
	g++ -c main.cpp
	
clean:
	rm -rf *.o main output *.csv *.png

run:
	./main
	rm -rf output
	mkdir output
	python process.py "Advection-Diffusion: Steep Gaussian"
	mv *.csv output
	mv *.png output
