/*================================== C++ ======================================*\
|                                                                               |
|                           1D Lattice-Boltzmann                                |
|                               Milo Feinberg                                   |
|                                                                               |
\*=============================================================================*/

//==============================================================================
//              USER INPUT DATA
//==============================================================================

// SIMULATION PARAMETERS
const bool WRITE = true;                // write out time-steps at writeInterval
const bool VERBOSE = false;             // write out time-steps at writeInterval
const bool NSE = false;                 // solves the Navier-Stokes equations if 
                                        // true, advection-diffusion if false.
                                        
const int writeInterval = 100;          // number of time-steps between writeouts
const int nTS = 4000;                   // number of time-steps

const int ICs = 3;                      // 1 -- density steps
                                        // 2 -- steep gaussian
                                        // 3 -- hyperbolic tangent

// DOMAIN PARAMETERS
const double L = 800.0;                 // length of the domain
const int N = 801;                      // number of elements in domain
const int BCs = 3;                      // 1 -- periodic B.C.
                                        // 2 -- bounce back B.C.
                                        // 3 -- Dirichlet B.C.

// FLUID PARAMETERS
const double cs = 1. / std::sqrt(3);    // the speed of sound
const double nu = 5e-8;                 // diffusion coefficient
// const double nu = 0.06;                 // diffusion coefficient

//==============================================================================
