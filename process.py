# -*- coding: utf-8 -*-
"""
Process results from LB 1D Solver
@author: Milo Feinberg
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import re
import sys

plt.style.use("seaborn-dark")


def tryint(s):
    try:
        return int(s)
    except:
        return s


def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]


files = os.listdir(os.getcwd())
files = [s for s in files if ".csv" in s]
files.sort(key=alphanum_key)


fig = plt.figure(figsize=(8,5), dpi=320)
ax = fig.subplots()
ax.grid(True)

N, X, rho, U = np.genfromtxt(files[0], delimiter=",", unpack=True, skip_header=1)
plt.plot(X, rho, label='initial time')

for file in files[1:-1]:
    N, X, rho, U = np.genfromtxt(file, delimiter=",", unpack=True, skip_header=1)
    plt.plot(X, rho, color="C0", linewidth=0.5, alpha=0.25)
    
N, X, rho, U = np.genfromtxt(files[-1], delimiter=",", unpack=True, skip_header=1)
plt.plot(X, rho, "-o", color="k", linewidth=1, markersize=2, label="final time")
    
ax.set(title=sys.argv[1], xlabel="X", ylabel="rho")
ax.legend(loc="best")
fig.savefig("plot-density.png", dpi=320, bbox_inches="tight")
