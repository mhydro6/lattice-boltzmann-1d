# Lattice Boltzmann 1D

Milo Feinberg

Code for solving the 1D Advection-Diffusion or 1D Navier-Stokes equations using the Lattice Boltzmann method.  Developed for Fluid Dynamics with the Lattice Boltzmann Method, Prof. Karlin, Fall 2021 ETH Zurich.

### Usage

Parameters are passed to the simulation through the `input.hpp` file.  Once these are set:

1. compile the program: `make`
2. run the program: `make run`

The output files and plots will be found in the sub-directory `output`.

The following branches have the input file pre-configured for the test cases from exercise 1.  They may be accessed by changing branches: `git branch <branch-name>`.

* `shock-tube` -- navier-stokes shock-tube case with bounce back boundary conditions.
* `steep-gaussian` -- advection-diffusion case with the Gaussian initial condition and periodic boundary conditions.
* `hyperbolic-tangent` -- advection-diffusion case with the hyerbolic tangent initial condition and Dirichlet boundary conditions.

### Dependencies

* GCC 9.3.0 or later
* GNU Make 4.2.1 or later
* Python 3, Numpy, Matplotlib -- (optional) for making graphs from the output files. Tested with Python 3.8.1, Numpy 1.20.1, and Matplotlib 3.3.4, but the script should work with any recent versions of these libraries.  If necessary, comment out line 14 in the `Makefile`.


### Results

#### Advection-Diffusion Equation: Steep Gaussian

![](img/advection_diffusion_steep_gaussian_density_profile.png)


#### Navier-Stokes Equation: 1D Shock-Tube

![](img/shock_tube_density_profile.png)
